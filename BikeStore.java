//Maria Maria Ramirez 2041788

package lab02;

public class BikeStore {
    public static void main (String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        
        Bicycle bike1 = new Bicycle("Banshee", 3, 19.0);
        Bicycle bike2 = new Bicycle("Altruiste", 2, 20.0);
        Bicycle bike3 = new Bicycle("Berg", 4, 21.0);
        Bicycle bike4 = new Bicycle("Brodie", 3, 22.0);
        bikes[0] = bike1;
        bikes[1] = bike2;
        bikes[2] = bike3;
        bikes[3] = bike4;

        for (int i = 0 ; i < bikes.length ; i++){
            System.out.println("Bike " + (i+1) + ": " + bikes[i].toString());
        }
    }
}
